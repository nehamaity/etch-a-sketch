const container = document.querySelector('#container');


function createCanvas(sideSquares) {
    for (let i = 0; i < (sideSquares*sideSquares); i++) {
        const div = document.createElement('div');
        div.classList.add('element');

        container.appendChild(div);
    }

    
};

createCanvas(16);

function userPrompt() {
    document.querySelectorAll('div').forEach(cell => { cell.style.backgroundColor = "white";})
    let promptSquares = prompt("Enter how many squares you'd like per side: (Max: 100)");
    let userSquares = parseInt(promptSquares, 16);
    
    createCanvas(userSquares);
}